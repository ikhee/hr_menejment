import 'package:flutter/material.dart';
import 'package:hr_menegment/Screens/Attendance/attendance_widget.dart';
import 'package:hr_menegment/Screens/AttendanceDetail/AttendanceDetail.dart';
import 'package:hr_menegment/Screens/UserList/user_list_screen.dart';
import 'package:hr_menegment/Screens/Welcome/welcome_screen.dart';
import 'Screens/Attendance/RequestAttend/generalAttendtReq.dart';
import 'Screens/Attendance/RequestAttend/requestAttend.dart';
import 'Screens/Attendance/RequestAttend/requestAttendList.dart';
import 'Screens/detailUser/detail_user_screen.dart';
import 'Screens/forgot_password/forgot_password.dart';
import 'Screens/Menu/menu_screen.dart';

final Map<String, WidgetBuilder> routes = {
  WelcomeScreen.routeName: (context) => WelcomeScreen(),
  ForgotPasswordScreen.routeName: (context) => ForgotPasswordScreen(),
  DetailUserScreen.routeName: (context) => DetailUserScreen(),
  MenuScreen.routeName: (context) => MenuScreen(),
  UserListScreen.routeName: (context) => UserListScreen(),
  AttendanceList.routeName: (context) => AttendanceList(),
  AttendanceDetail.routeName: (context) => AttendanceDetail(),
  AttendGeneralReq.routeName: (context) => AttendGeneralReq(),
  RequestAttend.routeName: (context) => RequestAttend(),
};