import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF0D42CC);
const kPrimaryLightColor = Color(0xFFF1E6FF);
const kPrimaryColor2 = Color(0xFF6F26B8);
const appName = "Хүний нөөцийн систем";

// Form Error
final RegExp emailValidatorRegExp =
RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
const String kUserNullError = "Нэвтрэх нэр оруулна уу.";
const String kMailNullError = "Мэйл хаяг оруулна уу.";
const String kPassNullError = "Нууц үг оруулна уу.";
const String kInValidMailError = "Зөв мэйл хаяг оруулна уу.";
const String notValidateUser = "Нэвтрэх боломжгүй байна.";