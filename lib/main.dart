import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hr_menegment/route.dart';
import 'package:hr_menegment/theme.dart';
import 'Screens/Welcome/welcome_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: theme(),
      home: WelcomeScreen(),
      routes: routes,
    );
  }
}