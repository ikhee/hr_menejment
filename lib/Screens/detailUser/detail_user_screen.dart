import 'package:flutter/material.dart';
import 'package:hr_menegment/Screens/UserList/components/detailUser.dart';
import 'package:hr_menegment/Screens/Welcome/components/background.dart';
import 'package:hr_menegment/Screens/detailUser/component/body.dart';

class DetailUserScreen extends StatelessWidget {
  static String routeName = "/detail_user";
  final DetailUser user;

  const DetailUserScreen({Key key, this.user}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: PreferredSize(
              preferredSize: Size.fromHeight(40.0),
              child: AppBar(
                title: Text("Ажилтны дэлгэрэнгүй", style: TextStyle(color: Colors.white),),
                iconTheme: IconThemeData(
                    color: Colors.white
                ),
                flexibleSpace: Background(
                  child: SafeArea(
                    child: Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: <Color>[
                            Colors.indigo[900],
                            Colors.deepPurple[800]
                          ]
                        )
                      )
                    ),
                  ),
                )
              )
          ),
          body: Body(detailUser: this.user,),
    );
  }
}

// ListView.builder(
// itemCount: 5,
// itemBuilder: (BuildContext context, int index){
// return ListTile(
// leading: Icon(Icons.list),
// trailing: Text("GFG",
//
// ),
// title: Text("List item $index"),
// );
// }
// ),