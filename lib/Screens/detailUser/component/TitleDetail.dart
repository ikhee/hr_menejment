class TitleDetail {
  final String title;
  final int titleIndex;

  TitleDetail(this.title, this.titleIndex);
}