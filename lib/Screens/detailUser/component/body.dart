import 'package:flutter/material.dart';
import 'package:hr_menegment/Screens/UserList/components/detailUser.dart';
import 'TitleDetail.dart';
import 'detail_component.dart';

class Body extends StatelessWidget {

  final titles = [
    'Үндсэн мэдээлэл', 'Нэмэлт мэдээлэл', 'Холбоо барих', 'Хаяг',
    'Гэр бүл, хамаатан садан', 'Бичиг баримт', 'Гадаад хэлний мэдлэг',
    'Боловсрол', 'Албан тушаал', 'Данс', 'Зээл', 'Тусламж, тэтгэмж',
    'Холбоотой үндсэн хөрөнгө', 'Ажилтны түүх', 'Ээлжийн бүртгэл', 'Сургалт'
  ];

  final icons = [IconData(58806, fontFamily: 'MaterialIcons'), IconData(59748, fontFamily: 'MaterialIcons'), Icons.contact_phone, Icons.contacts,
    IconData(61000, fontFamily: 'MaterialIcons'), Icons.book, Icons.language,
    Icons.school, Icons.business, Icons.account_balance_wallet, Icons.monetization_on, Icons.local_hospital,
    Icons.laptop, Icons.history, IconData(58785, fontFamily: 'MaterialIcons'), Icons.art_track
  ];

  final DetailUser detailUser;

  Body({Key key, this.detailUser}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomPadding: true,
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 0),
                  child: Image.asset("assets/images/detail_top.png",),
                ),
              ],
            ),
            SingleChildScrollView(
              physics: ScrollPhysics(),
              child: Padding(
                padding: EdgeInsets.only(left: 28, right: 28, top: 30),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: [
                        Image.asset(
                          "assets/icons/user_icon.png",
                          width: size.height*0.15,
                        ),
                        SizedBox(width: size.width*0.050,),
                        Column(
                          children: [
                            Text(
                              this.detailUser.firstName,
                              style: new TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                                shadows: [
                                  Shadow(
                                    blurRadius: 16,
                                    color: Colors.white,
                                    offset: Offset(0, 1)
                                  )
                                ]
                              ),
                            ),
                            Text(
                                this.detailUser.positionName,
                                style: new TextStyle(
                                    fontWeight: FontWeight.w700,
                                    fontStyle: FontStyle.italic,
                                    fontSize: 16,
                                    shadows: [
                                      Shadow(
                                          blurRadius: 16,
                                          color: Colors.white,
                                          offset: Offset(0, 1)
                                      )
                                    ]
                                ),
                            )
                          ],
                        ),
                      ],
                    ),
                    ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true, // use it
                      itemCount: titles.length,
                      itemBuilder: (context, index) {
                        return Card( //                           <-- Card widget
                          child: ListTile(
                            leading: Icon(
                                icons[index],
                                color: Colors.deepPurple[700],
                            ),
                            title: Text(titles[index]),
                            onTap: () { //                                  <-- onTap
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => DetailComponent(titleDetail: detailInfo(index), detailUser: detailUser,),
                                ),
                              );
                            },
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
    );
  }

  TitleDetail detailInfo(int index) {
    TitleDetail titleDetail = new TitleDetail(titles[index], index);
    print(titles[index]);
    return titleDetail;
  }
}
