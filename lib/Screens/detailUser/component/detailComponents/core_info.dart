import 'package:flutter/material.dart';
import 'package:hr_menegment/Screens/UserList/components/detailUser.dart';

class CoreInfo extends StatelessWidget {
  final DetailUser detailUser;

  const CoreInfo({Key key, this.detailUser}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      padding: new EdgeInsets.all(16),
      width: size.width,
      child: Column(
        children: [
          Card(
            semanticContainer: true,
            elevation: 5,
            clipBehavior: Clip.antiAliasWithSaveLayer,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.network(
                      "https://cdn3.iconfinder.com/data/icons/vector-icons-6/96/256-512.png",
                      height: size.height * 0.2,
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Column(
                      children: [
                        Text(
                          detailUser.lastName != null ? detailUser.lastName : "GrapeCity",
                          style: new TextStyle(
                              fontSize: 17, fontWeight: FontWeight.normal),
                        ),
                        Text(
                          detailUser.firstName,
                          style: new TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.italic
                          ),
                        ),
                        Text(
                          detailUser.positionName,
                          style: new TextStyle(
                              fontSize: 17, fontWeight: FontWeight.normal),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 16,
                ),
                Text(
                    detailUser.chartName != null ? detailUser.chartName: "СИСТЕМ ИНТЕГРАЦИЙН ГАЗАР",
                    style: new TextStyle(
                      fontSize: 17, fontWeight: FontWeight.w500),
                ),
                Text(
                  detailUser.statusName != null ? detailUser.statusName: "Ажиллаж байгаа",
                  style: new TextStyle(
                      fontSize: 13, fontWeight: FontWeight.w100),
                ),
              ],
            ),
          ),
          DetailItem(
            title: "Дугаар",
            body: detailUser.personCode != null ? detailUser.personCode : "***",
            iconData: IconData(59644, fontFamily: 'MaterialIcons'),
          ),
          DetailItem(
            title: "Регистер",
            body: detailUser.registerCode != null ? detailUser.registerCode : "АБ99071111",
            iconData: IconData(58785, fontFamily: 'MaterialIcons'),
          ),
          DetailItem(
            title: "Утас",
            body: detailUser.phone != null ? detailUser.phone : "9911****",
            iconData: Icons.call,
          ),
          DetailItem(
            title: "Имэйл хаяг",
            body: detailUser.workEmail != null ? detailUser.workEmail : "***@grapecity.mn",
            iconData: IconData(59123, fontFamily: 'MaterialIcons'),
          ),
          DetailItem(
            title: "Зэрэг дэв",
            body: detailUser.rankName != null ? detailUser.rankName : "Ахлах",
            iconData: IconData(60148, fontFamily: 'MaterialIcons'),
          ),
        ],
      ),
    );
  }
}

class DetailItem extends StatelessWidget {
  final String title, body;
  final IconData iconData;

  const DetailItem({
    Key key,
    this.title,
    this.body = "GCM",
    this.iconData = const IconData(59108, fontFamily: 'MaterialIcons'),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      hoverColor: Colors.blue,
        leading: Icon(
          iconData,
          color: Colors.deepPurple[700],
        ),
        title: Text(title),
        subtitle: Text(body),
    );
  }
}
