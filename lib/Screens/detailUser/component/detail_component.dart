import 'package:flutter/material.dart';
import 'package:hr_menegment/Screens/UserList/components/detailUser.dart';
import 'package:hr_menegment/Screens/detailUser/component/detailComponents/address.dart';
import 'package:hr_menegment/Screens/detailUser/component/detailComponents/contact.dart';
import 'package:hr_menegment/Screens/detailUser/component/detailComponents/core_info.dart';
import 'package:hr_menegment/Screens/detailUser/component/detailComponents/more_info.dart';
import 'package:hr_menegment/components/doing_develop.dart';

import 'TitleDetail.dart';

class DetailComponent extends StatelessWidget {
  final TitleDetail titleDetail;
  final DetailUser detailUser;
  final String routeName = "/detailComponent";

  DetailComponent({Key key, @required this.titleDetail, this.detailUser}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(titleDetail.title),
      ),
      body: Container(
        width: double.infinity,
        height: size.height,
        child: Stack(
          children: [
            SingleChildScrollView(
              physics: ScrollPhysics(),
              child: detailComponent(this.titleDetail),
            ),
            Positioned(
                bottom: 0,
                right: 0,
                child: Image.asset(
                    "assets/images/right_bottom.png",
                  width: size.width*1.2,
                )
            )
          ],
        ),
      )
    );
  }

  detailComponent(TitleDetail detail) {
    switch (detail.titleIndex) {
      case 0:
        return CoreInfo(detailUser: detailUser,);
        break;
      // case 1:
      //   return MoreInfo();
      //   break;
      // case 2:
      //   return Contact();
      //   break;
      // case 3:
      //   return Address();
      //   break;
      default:
        return DoingDevelop();
    }
  }
}
