import 'package:flutter/material.dart';
import 'package:hr_menegment/Screens/Attendance/Components/body.dart';

class AttendanceList extends StatelessWidget {
  static String routeName = "/attendance";
  String uidFromMenu;

  @override
  Widget build(BuildContext context) {
    uidFromMenu = ModalRoute.of(context).settings.arguments as String;

    return Scaffold(
      appBar: AppBar(
        title: Text('Ирцийн хуудасны лавлагаа'),
      ),
      body: BodyOfAttendanceList(uidFromAttendance: uidFromMenu,),
    );
  }
}
