import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class DetailAttendance {
  DateTime att_date;
  int att_awanting_minutes;
  DateTime att_end_time;
  DateTime att_start_time;
  String ref;

  DetailAttendance(
      {this.att_date,
      this.att_awanting_minutes,
      this.att_end_time,
      this.att_start_time,
      this.ref});

  DetailAttendance.fromJson(Map<String, dynamic> json)
      : att_date = DateTime.parse(json['att_date'].toString() + 'T000000'),
        att_awanting_minutes = json['att_awanting_minutes'],
        att_end_time = DateTime.parse(json['att_date'].toString() +
            'T' +
            json['att_end_time'].toString().replaceAll(':', '') +
            '00'),
        att_start_time = DateTime.parse(json['att_date'].toString() +
            'T' +
            json['att_start_time'].toString().replaceAll(':', '') +
            '00'),
        ref = json['ref'];

  getLeaveHours() {
    if (this.att_awanting_minutes != null) {
      int h = (att_awanting_minutes / 60).floor();
      int m = att_awanting_minutes % 60;
      if (h == 0) {
        return m.toString() + " мин";
      } else {
        return h.toString() + " ц " + m.toString() + " мин";
      }
    } else {
      return "0 мин";
    }
  }
}
