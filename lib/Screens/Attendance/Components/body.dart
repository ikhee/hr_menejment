import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:hr_menegment/Screens/Attendance/RequestAttend/AttendRequestClass.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

import 'package:hr_menegment/Screens/Attendance/RequestAttend/requestAttend.dart';
import 'package:hr_menegment/components/progress_dialog.dart';
import 'package:hr_menegment/components/DateTimeToDateObj.dart';
import '../AttendanceDetail.dart';

class BodyOfAttendanceList extends StatefulWidget {
  String uidFromAttendance;

  BodyOfAttendanceList({this.uidFromAttendance});

  @override
  _BodyOfAttendanceListState createState() =>
      _BodyOfAttendanceListState(uid: uidFromAttendance);
}

class _BodyOfAttendanceListState extends State<BodyOfAttendanceList> {
  List<DetailAttendance> attendances = new List();
  bool sort = true;
  String uid;

  _BodyOfAttendanceListState({this.uid});

  getAttendanceList(uid) async {
    final http.Response response = await http.post(
        'https://us-central1-hr-management-4c06c.cloudfunctions.net/getAttendanceList',
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'uid': uid,
        }));
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      print('statusCode: ${response.statusCode} \n body: ${response.body}');
    }
  }

  @override
  void initState() {
    Future.delayed(Duration.zero, () async {
      uid = ModalRoute.of(context).settings.arguments as String;

      ProgressDialog pr = ProgressDialog(context,
          type: ProgressDialogType.Normal,
          isDismissible: false,
          showLogs: false);

      pr.style(
          message: 'Уншиж байна...',
          borderRadius: 10.0,
          backgroundColor: Colors.white,
          progressWidget: CircularProgressIndicator(),
          elevation: 10.0,
          insetAnimCurve: Curves.easeInOut,
          progress: 0.0,
          maxProgress: 100.0,
          progressTextStyle: TextStyle(
              color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
          messageTextStyle: TextStyle(
              color: Colors.black,
              fontSize: 19.0,
              fontWeight: FontWeight.w600));

      pr.show();
      dynamic data = await getAttendanceList(uid);
      print(data);
      final List<DetailAttendance> loadingList = List();
      for (dynamic i in data) {
        loadingList.add(DetailAttendance.fromJson(i));
      }
      setState(() {
        attendances = loadingList;
        pr.hide();
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: size.width,
      child: Center(
        child: Container(
          color: Colors.white,
          height: size.height * 0.9,
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: DataTable(
                sortColumnIndex: 0,
                sortAscending: sort,
                columns: <DataColumn>[
                  DataColumn(
                    label: Text('Огноо'),
                    onSort: (columnIndex, ascending) {
                      onSortColumn(columnIndex, ascending);
                      setState(() {
                        sort = !sort;
                      });
                    },
                  ),
                  DataColumn(
                    label: Text('Эхлэх цаг'),
                  ),
                  DataColumn(
                    label: Text('Дуусах цаг'),
                  ),
                  DataColumn(
                    label: Text('Дутуу цаг'),
                  ),
                  DataColumn(
                    label: Text('Хүсэлт'),
                  )
                ],
                rows: getDataAttendance(attendances),
              ),
            ),
          ),
        ),
      ),
    );
  }

  List<DataRow> getDataAttendance(List<DetailAttendance> data) {
    List<DataRow> rows = [];
    data.sort((a, b) => a.att_start_time.compareTo(b.att_start_time));
    data.forEach((element) {
      Icon chosen;
      int status;

      if (element.ref == null) {
        status = 0;
        chosen = Icon(Icons.add_circle, color: Colors.green);
      } else if (element.ref == 'done') {
        status = 2;
        chosen = Icon(Icons.done, color: Colors.green);
      } else {
        status = 1;
        chosen = Icon(Icons.warning, color: Colors.yellow);
      }

      rows.add(DataRow(cells: [
        DataCell(Text(DateObject(element.att_start_time).getDateString())),
        DataCell(Text(DateFormat.Hm().format(element.att_start_time))),
        DataCell(Text(DateFormat.Hm().format(element.att_end_time))),
        DataCell(Text(element.getLeaveHours())),
        DataCell(chosen, onTap: () {
          print(status);
          RequestAttendance requestClass = new RequestAttendance();
          requestClass.startDate = element.att_start_time;
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context1) => RequestAttend(
                        attendanceClass: requestClass,
                        uid: uid,
                        ref: element.ref,
                        status: status,
                      )));
        })
      ]));
    });
    return rows;
  }

  onSortColumn(int columnIndex, bool ascending) {
    if (columnIndex == 0) {
      if (ascending) {
        attendances
            .sort((a, b) => a.att_start_time.compareTo(b.att_start_time));
      } else {
        attendances
            .sort((a, b) => b.att_start_time.compareTo(a.att_start_time));
      }
    }
  }
}
