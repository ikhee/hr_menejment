import 'package:flutter/material.dart';
import 'package:hr_menegment/Screens/Attendance/RequestAttend/requestAttend.dart';
import 'package:hr_menegment/Screens/Attendance/RequestAttend/requestAttendList.dart';

class AttendGeneralReq extends StatelessWidget {
  static String routeName = "/request_attend_list";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Амралт чөлөөний хүсэлт"),
      ),
      body: RequestAttendList(),
      floatingActionButton: FloatingActionButton(
        onPressed: () => {
          Navigator.pushNamed(context, RequestAttend.routeName)
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
