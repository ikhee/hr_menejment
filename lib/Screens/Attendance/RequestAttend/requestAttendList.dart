import 'package:flutter/material.dart';
import 'package:hr_menegment/Screens/Attendance/RequestAttend/requestAttend.dart';
import 'package:hr_menegment/components/DateTimeToDateObj.dart';

import 'AttendRequestClass.dart';

class RequestAttendList extends StatefulWidget {
  @override
  _RequestAttendListState createState() => _RequestAttendListState();
}

class _RequestAttendListState extends State<RequestAttendList> {
  bool sort = true;
  final List<RequestAttendance> attendances = [];
  final List dataList = [
    {
      "startDate": "2020-11-23T00:00:00",
      "term": 1,
      "termUnitName": "Өдөр",
      "leaveTypeName": "Гадуур ажилласан",
      "isSalary": "Y",
      "leaveTypeId": 3,
      "statusName": "Биелсэн",
      "termUnitId": "D"
    },
    {
      "startDate": "2020-11-24T00:00:00",
      "term": 14,
      "termUnitName": "Минут",
      "leaveTypeName": "Гадуур ажилласан",
      "isSalary": "Y",
      "leaveTypeId": 1,
      "statusName": "Биелсэн",
      "termUnitId": "MI"
    },
    {
      "startDate": "2020-11-25T00:00:00",
      "term": 2,
      "termUnitName": "Өдөр",
      "leaveTypeName": "Чөлөөтэй",
      "isSalary": "Y",
      "leaveTypeId": 7,
      "statusName": "Биелсэн",
      "termUnitId": "D"
    }
  ];

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    initData();
    return Container(
      height: size.height,
      width: size.width,
      child: Center(
        child: Container(
          color: Colors.white,
          height: size.height * 0.9,
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: DataTable(
                sortColumnIndex: 0,
                sortAscending: sort,
                columns: <DataColumn>[
                  DataColumn(
                    label: Text('Огноо'),
                    onSort: (columnIndex, ascending) {
                      onSortColumn(columnIndex, ascending);
                      setState(() {
                        sort = !sort;
                      });
                    },
                  ),
                  DataColumn(
                    label: Text('Төрөл'),
                  ),
                  DataColumn(
                    label: Text('Хугацаа'),
                  ),
                  DataColumn(
                    label: Text('Төлөв'),
                  ),
                  DataColumn(
                    label: Text('Хүсэлт'),
                  )
                ],
                rows: getDataAttendance(attendances),
              ),
            ),
          ),
        ),
      ),
    );
  }

  initData() {
    attendances.clear();
    dataList.forEach((element) {
      RequestAttendance detail = RequestAttendance.fromJson(element);
      attendances.add(detail);
    });
  }

  onSortColumn(int columnIndex, bool ascending) {
    print(
        columnIndex.toString() + " " + (ascending == true ? "true" : "false"));
    if (columnIndex == 0) {
      if (ascending) {
        attendances.sort((a, b) => a.startDate.compareTo(b.startDate));
      } else {
        attendances.sort((a, b) => b.startDate.compareTo(a.startDate));
      }
    }
  }

  List<DataRow> getDataAttendance(List<RequestAttendance> data) {
    List<DataRow> rows = [];
    data.forEach((element) {
      rows.add(DataRow(cells: [
        DataCell(Text(DateObject(element.startDate).getDateString())),
        DataCell(Text(element.leaveTypeName)),
        DataCell(Text(element.getTermFullString())),
        DataCell(Text(element.statusName)),
        DataCell(
            Icon(
              IconData(58878, fontFamily: 'MaterialIcons'),
              color: Colors.deepPurple,
            ), onTap: () {
          print("Төлөв. " + element.statusName);
          Navigator.push(context,
            MaterialPageRoute(
            builder: (context) => RequestAttend(attendanceClass: element,),
          ),);
        })
      ]));
    });
    return rows;
  }
}
