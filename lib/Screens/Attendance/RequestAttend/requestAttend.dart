import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;

import 'package:hr_menegment/components/DateTimeToDateObj.dart';
import 'package:hr_menegment/components/column_surfix_icon.dart';
import 'package:hr_menegment/components/dropdown.dart';
import 'package:hr_menegment/components/initClass/dropdown_data.dart';
import 'package:hr_menegment/components/rounded_button.dart';
import '../../../constants.dart';
import 'AttendRequestClass.dart';

class RequestAttend extends StatefulWidget {
  final RequestAttendance attendanceClass;
  final String uid;
  final int status;
  final String ref;

  const RequestAttend(
      {Key key, this.attendanceClass, this.uid, this.status, this.ref})
      : super(key: key);
  static String routeName = "/request_attend";

  @override
  _RequestAttendState createState() =>
      _RequestAttendState(uid, this.attendanceClass, status, ref);
}

class _RequestAttendState extends State<RequestAttend> {
  String uid;
  String ref;
  int status;
  List<DropdownData> leaveTypeList = [
    DropdownData("1", "Ээлжийн амралт"),
    DropdownData("2", "Чөлөө"),
    DropdownData("3", "Гадуур ажилласан"),
    DropdownData("4", "Томилолт"),
    DropdownData("5", "Сургалт"),
    DropdownData("6", "Өвчтэй"),
    DropdownData("7", "Тасалсан"),
  ];

  List<DropdownData> unitTimeList = [
    DropdownData("MI", "Минут"),
    DropdownData("T", "Цаг"),
    DropdownData("D", "Өдөр"),
    DropdownData("M", "Сар"),
    DropdownData("Y", "Жил")
  ];

  List<DropdownMenuItem<DropdownData>> _modelDropdownList;
  List<DropdownMenuItem<DropdownData>> _unitTimeModelDropdownList;
  DropdownData _model;
  DropdownData _unitTimeModel;
  FocusNode myFocusNode = new FocusNode();
  String term;
  bool timeDisable = false;
  bool startDateDisable = false;
  TextEditingController startDate = TextEditingController();
  TextEditingController endDate = TextEditingController();
  TextEditingController txtStartTime = TextEditingController();
  TextEditingController txtEndTime = TextEditingController();
  TextEditingController termInput = TextEditingController();
  RequestAttendance _requestAttendance = RequestAttendance();

  _RequestAttendState(String userId, RequestAttendance attendanceClass,
      int statusFromList, String refFromList) {
    uid = userId;
    ref = refFromList;
    status = statusFromList;
    if (statusFromList == 0) {
      if (attendanceClass != null) {
        _requestAttendance = attendanceClass;
        termInput.text = attendanceClass.term != null
            ? attendanceClass.term.toString()
            : '0';
        if (attendanceClass.startDate != null) {
          startDate.text =
              DateObject(attendanceClass.startDate).getDatePickerString();
          txtStartTime.text = DateFormat.Hm().format(attendanceClass.startDate);
          if (_requestAttendance.term != null &&
              _requestAttendance.termUnitId != null) {
            DateTime datetime = getFinishTime(_requestAttendance.termUnitId,
                _requestAttendance.term, attendanceClass.startDate);
            setFinishTime(datetime);
            unitTimeList.forEach((element) {
              if (element.id == _requestAttendance.termUnitId) {
                _unitTimeModel = element;
                checkDateTimes(element.id);
              }
            });
          } else {
            _unitTimeModel = unitTimeList[2];
          }
        } else {
          startDate.text = DateObject(DateTime.now()).getDatePickerString();
          txtStartTime.text = DateObject(DateTime.now()).getTimeMinuteString();
        }

        if (_requestAttendance.leaveTypeId != null) {
          leaveTypeList.forEach((element) {
            if (element.id == _requestAttendance.leaveTypeId.toString()) {
              _model = element;
            }
          });
        } else {
          _model = leaveTypeList[0];
        }
      } else {
        _unitTimeModel = unitTimeList[2];
        _model = leaveTypeList[0];
        startDate.text = DateObject(DateTime.now()).getDatePickerString();
        txtStartTime.text = DateObject(DateTime.now()).getTimeMinuteString();
        checkDateTimes(_unitTimeModel.id);
      }
    }
  }

  detailVacation(ref) async {
    final http.Response response = await http.post(
        'https://us-central1-hr-management-4c06c.cloudfunctions.net/detailVacation',
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'ref': ref,
        }));
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      print('statusCode: ${response.statusCode} \n body: ${response.body}');
    }
  }

  List<DropdownMenuItem<DropdownData>> _buildModelDropdown(List modelList) {
    List<DropdownMenuItem<DropdownData>> items = List();
    for (DropdownData model in modelList) {
      items.add(DropdownMenuItem(
        value: model,
        child: Text(model.name),
      ));
    }
    return items;
  }

  DateTime getFinishTime(String termId, int termUnit, DateTime startTime) {
    switch (termId) {
      case 'MI':
        startTime = startTime.add(new Duration(minutes: termUnit));
        break;
      case 'T':
        startTime = startTime.add(new Duration(hours: termUnit));
        break;
      case 'D':
        startTime = startTime.add(new Duration(days: termUnit));
        break;
      case 'M':
        startTime = new DateTime(
            startTime.year, startTime.month + termUnit, startTime.day);
        break;
      case 'Y':
        startTime = new DateTime(
            startTime.year + termUnit, startTime.month, startTime.day);
        break;
    }
    return startTime;
  }

  setFinishTime(DateTime finishTime) {
    endDate.text = DateObject(finishTime).getDatePickerString();
    txtEndTime.text = DateFormat.Hm().format(finishTime);
  }

  _onChangeModelDropdown(DropdownData model) {
    setState(() {
      _model = model;
    });
  }

  _onChangeUnitTimeDropdown(DropdownData model) {
    setState(() {
      _unitTimeModel = model;
      checkDateTimes(model.id);
      if (termInput.text != null) {
        onChangeTerm(termInput.text);
      }
    });
  }

  checkDateTimes(String id) {
    if (id == 'MI' || id == 'T') {
      this.timeDisable = true;
      this.startDateDisable = false;
    } else {
      txtStartTime.text = "0:0";
      this.timeDisable = false;
      this.startDateDisable = true;
    }
  }

  void selectTime(BuildContext context, dynamic timeVar) async {
    TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );

    if (picked != null) {
      setState(() {
        timeVar.text = (picked.hour < 10
                ? ('0' + picked.hour.toString())
                : picked.hour.toString()) +
            ':' +
            (picked.minute < 10
                ? ('0' + picked.minute.toString())
                : picked.minute.toString());
      });
      onChangeTime(timeVar.text);
    }
  }

  void selectDate(BuildContext context, dynamic dateController) async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2020, 1),
        lastDate: DateTime(2021, 12));

    if (picked != null) {
      setState(() {
        print(picked);
        dateController.text = picked.toString().substring(0, 10);
        onChangeStartDate(dateController.text);
      });
    }
  }

  onChangeTerm(String value) {
    if (value != '') {
      DateTime finishTime = getFinishTime(
          _unitTimeModel.id, int.parse(value), DateTime.parse(startDate.text));
      setFinishTime(finishTime);
    }
  }

  onChangeTime(String value) {
    String stDate =
        DateObject(DateTime.parse(startDate.text)).getDatePickerString();
    onChangeStartDate(stDate + "T" + value);
  }

  onChangeStartDate(String value) {
    DateTime finishTime = getFinishTime(
        _unitTimeModel.id, int.parse(termInput.text), DateTime.parse(value));
    setFinishTime(finishTime);
  }

  saveRequest(Map<String, dynamic> request) async {
    final http.Response response = await http.post(
        'https://us-central1-hr-management-4c06c.cloudfunctions.net/saveRequestOfVacation',
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(request));
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      print('statusCode: ${response.statusCode} \n body: ${response.body}');
    }
  }

  @override
  void initState() {
    super.initState();
    _modelDropdownList = _buildModelDropdown(leaveTypeList);
    _unitTimeModelDropdownList = _buildModelDropdown(unitTimeList);
    print(status);
    if (status == 1) {
      dynamic valueOfUnitTimeModel, valueOfModel;
      Future.delayed(Duration.zero, () async {
        dynamic data = await detailVacation(ref);
        print(data);
        _requestAttendance = RequestAttendance.fromJson(data);
        if (_requestAttendance != null) {
          _requestAttendance = _requestAttendance;
          termInput.text = _requestAttendance.term != null
              ? _requestAttendance.term.toString()
              : '0';
          if (_requestAttendance.startDate != null) {
            startDate.text =
                DateObject(_requestAttendance.startDate).getDatePickerString();
            txtStartTime.text =
                DateFormat.Hm().format(_requestAttendance.startDate);
            if (_requestAttendance.term != null &&
                _requestAttendance.termUnitId != null) {
              DateTime datetime = getFinishTime(_requestAttendance.termUnitId,
                  _requestAttendance.term, _requestAttendance.startDate);
              setFinishTime(datetime);
              unitTimeList.forEach((element) {
                if (element.id == _requestAttendance.termUnitId) {
                  valueOfUnitTimeModel = element;
                  checkDateTimes(element.id);
                }
              });
            } else {
              valueOfUnitTimeModel = unitTimeList[2];
            }
          } else {
            startDate.text = DateObject(DateTime.now()).getDatePickerString();
            txtStartTime.text =
                DateObject(DateTime.now()).getTimeMinuteString();
          }

          if (_requestAttendance.leaveTypeId != null) {
            leaveTypeList.forEach((element) {
              if (element.id == _requestAttendance.leaveTypeId.toString()) {
                valueOfModel = element;
              }
            });
          } else {
            valueOfModel = leaveTypeList[0];
          }

          setState(() {
            _unitTimeModel = valueOfUnitTimeModel;
            _model = valueOfModel;
          });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    String txtOfBtn = 'Илгээх';

    if (status == 1) {
      txtOfBtn = 'Зөвшөөрөх';
    }
    return Scaffold(
      appBar: AppBar(
        title: Text("Амралт чөлөөний хүсэлт"),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                width: double.infinity,
                child: Text(
                  'Төрөл:',
                  style: TextStyle(color: kPrimaryColor),
                ),
              ),
              CustomDropdown(
                dropdownMenuItemList: _modelDropdownList,
                onChanged: _onChangeModelDropdown,
                value: _model,
                isEnabled: true,
              ),
              SizedBox(height: size.height * 0.02),
              SizedBox(
                width: double.infinity,
                child: Text(
                  'Дэлгэрэнгүй:',
                  style: TextStyle(color: kPrimaryColor),
                ),
              ),
              SizedBox(height: size.height * 0.005),
              TextField(
                minLines: 2,
                maxLines: 8,
                autocorrect: false,
                decoration: InputDecoration(
                  hintText: 'Дэлгэрэнгүй мэдээллээ оруулна уу.',
                  suffixIcon: CustomSurfFixIcon(
                    svgIcon: "assets/icons/mail.svg",
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(30.0)),
                    borderSide: BorderSide(color: kPrimaryColor),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: kPrimaryColor),
                  ),
                ),
              ),
              SizedBox(height: size.height * 0.02),
              SizedBox(
                width: double.infinity,
                child: Text(
                  'Хугацааны нэгж:',
                  style: TextStyle(color: kPrimaryColor),
                ),
              ),
              SizedBox(height: size.height * 0.005),
              CustomDropdown(
                dropdownMenuItemList: _unitTimeModelDropdownList,
                onChanged: _onChangeUnitTimeDropdown,
                value: _unitTimeModel,
                isEnabled: true,
              ),
              SizedBox(height: size.height * 0.02),
              TextFormField(
                focusNode: myFocusNode,
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  onChangeTerm(value);
                },
                validator: (value) {
                  return null;
                },
                controller: termInput,
                decoration: InputDecoration(
                  labelText: "Хугацаа",
                  hintText: "Хугацаа оруулна уу.",
                  suffixIcon: CustomSurfFixIcon(
                    svgIcon: "assets/icons/time.svg",
                  ),
                ),
              ),
              SizedBox(height: size.height * 0.02),
              TextFormField(
                enabled: startDateDisable,
                decoration: InputDecoration(
                  hintText: 'Эхлэх огноо',
                  labelText: 'Эхлэх огноо',
                  suffixIcon: CustomSurfFixIcon(
                    svgIcon: "assets/icons/calendar.svg",
                  ),
                ),
                onTap: () => selectDate(context, startDate),
                controller: startDate,
                readOnly: true,
              ),
              SizedBox(height: size.height * 0.02),
              TextFormField(
                enabled: false,
                decoration: InputDecoration(
                  hintText: 'Дуусах огноо',
                  labelText: 'Дуусах огноо',
                  suffixIcon: CustomSurfFixIcon(
                    svgIcon: "assets/icons/calendar.svg",
                  ),
                ),
                onTap: () => selectDate(context, endDate),
                controller: endDate,
                readOnly: true,
              ),
              SizedBox(height: size.height * 0.02),
              TextFormField(
                enabled: timeDisable,
                decoration: InputDecoration(
                  hintText: 'Эхлэх цаг',
                  labelText: 'Эхлэх цаг',
                  suffixIcon: CustomSurfFixIcon(
                    svgIcon: "assets/icons/time1.svg",
                  ),
                ),
                onTap: () => selectTime(context, txtStartTime),
                controller: txtStartTime,
                readOnly: true,
              ),
              SizedBox(height: size.height * 0.02),
              TextFormField(
                enabled: false,
                decoration: InputDecoration(
                  hintText: 'Дуусах цаг',
                  labelText: 'Дуусах цаг',
                  suffixIcon: CustomSurfFixIcon(
                    svgIcon: "assets/icons/time1.svg",
                  ),
                ),
                onTap: () => selectTime(context, txtEndTime),
                controller: txtEndTime,
                readOnly: true,
              ),
              RoundedButton(
                text: txtOfBtn,
                press: () {
                  sendRequestAttend();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  sendRequestAttend() async {
    _requestAttendance.startDate =
        DateTime.parse(startDate.text + "T" + txtStartTime.text + ":00");
    _requestAttendance.leaveTypeName = _model.name;
    _requestAttendance.leaveTypeId = int.parse(_model.id);
    _requestAttendance.isSalary = "Y";
    _requestAttendance.term = int.parse(termInput.text);
    _requestAttendance.termUnitName = _unitTimeModel.name;
    _requestAttendance.termUnitId = _unitTimeModel.id;
    _requestAttendance.UID = uid;
    print(_requestAttendance);
    dynamic resDate = await saveRequest(_requestAttendance.toJson());
    print(resDate);
  }
}
