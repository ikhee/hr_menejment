import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class RequestAttendance {
  DateTime startDate;
  int term;
  String termUnitName;
  String termUnitId;
  String leaveTypeName;
  int leaveTypeId;
  String isSalary;
  String statusName;
  String statusCode;
  int requestId;
  String UID;

  RequestAttendance();

  RequestAttendance.fromJson(Map<String, dynamic> json)
      : startDate = DateTime.parse(json['startDate']),
        term = json['term'],
        termUnitName = json['termUnitName'],
        termUnitId = json['termUnitId'],
        leaveTypeName = json['leaveTypeName'],
        leaveTypeId = json['leaveTypeId'],
        statusName = json['statusName'],
        statusCode = json['statusCode'],
        requestId = json['requestId'],
        isSalary = json['isSalary'];

  Map<String, dynamic> toJson() => {
    'startDate': startDate.toIso8601String(),
    'term': term,
    'termUnitName': termUnitName,
    'termUnitId': termUnitId,
    'leaveTypeName': leaveTypeName,
    'leaveTypeId': leaveTypeId,
    'statusName': statusName,
    'statusCode': statusCode,
    'requestId': requestId,
    'isSalary': isSalary,
    'UID': UID,
  };

  getTermFullString() {
    return term.toString() + " " + termUnitName;
  }

  String getFullInfo() {
    return startDate.toString() +
        "\n Хугацаа " +
        term.toString() +
        " " +
        termUnitName +
        " " +
        termUnitId +
        " \nТөлөв " +
        statusName +
        " " +
        statusCode + "\n Хүсэлтийн дугаар "+
        requestId.toString() +
        "\nЦалин" +
        isSalary;
  }
}
