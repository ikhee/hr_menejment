import 'dart:convert';
import 'dart:math';
import 'package:hr_menegment/Screens/AttendanceDetail/Components/UserWithUID.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:cool_alert/cool_alert.dart';

import 'package:hr_menegment/components/progress_dialog.dart';
import 'package:hr_menegment/components/rounded_button.dart';

class AttendanceDetail extends StatefulWidget {
  static String routeName = "/attendanceDetail";

  @override
  _AttendanceDetailState createState() => _AttendanceDetailState();
}

class _AttendanceDetailState extends State<AttendanceDetail> {
  String signedUID;
  String chosenUID;
  List<DropdownMenuItem<String>> listDrpItem;
  List<UserWithUID> listOfUIDs = new List();

  String dropdownValue;
  TextEditingController txtDateTime = TextEditingController();
  TextEditingController txtStartTime = TextEditingController();
  TextEditingController txtEndTime = TextEditingController();

  getDivUserUIDs(uid) async {
    final http.Response response = await http.post(
        'https://us-central1-hr-management-4c06c.cloudfunctions.net/getDivUserUIDs',
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'uid': uid,
        }));
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      print('statusCode: ${response.statusCode} \n body: ${response.body}');
    }
  }

  getDetailUser(uid) async {
    final http.Response response = await http.post(
        'https://us-central1-hr-management-4c06c.cloudfunctions.net/getDetailUser',
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'uid': uid,
        }));
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      print('statusCode: ${response.statusCode} \n body: ${response.body}');
    }
  }

  saveAttendance(uid) async {
    ProgressDialog pr = ProgressDialog(context,
        type: ProgressDialogType.Normal,
        isDismissible: false,
        showLogs: false);

    pr.style(
        message: 'Уншиж байна...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black,
            fontSize: 19.0,
            fontWeight: FontWeight.w600));

    pr.show();

    final http.Response response = await http.post(
        'https://us-central1-hr-management-4c06c.cloudfunctions.net/saveAttendance',
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'uid': uid,
          'att_date': txtDateTime.value.text.replaceAll('-', ''),
          'att_start_time': txtStartTime.value.text,
          'att_end_time': txtEndTime.value.text,
        })
    );

    pr.hide();
    if (response.statusCode == 200) {
      CoolAlert.show(
        context: context,
        type: CoolAlertType.success,
        text: "Амжилттай хадгаллаа",
      );
      return jsonDecode(response.body);
    } else {
      CoolAlert.show(
        context: context,
        type: CoolAlertType.error,
        text: response.body,
      );
      print('statusCode: ${response.statusCode} \n body: ${response.body}');
    }
  }

  void checkAllHasValue() {
    if (txtDateTime.value == null) {
      _showAlertDialog('Анхаар', 'Огноо талбарт утга оруулна уу.');
    } else {
      saveAttendance(chosenUID);
    }
  }

  void selectDate(BuildContext context, dynamic dateController) async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2020, 1),
        lastDate: DateTime(2021, 12));

    if (picked != null) {
      setState(() {
        dateController.text = picked.toString().substring(0, 10);
      });
    }
  }

  void selectTime(BuildContext context, dynamic timeVar, String when) async {
    TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );

    if (picked != null) {
      if (when == 'end') {
        if (int.parse(txtStartTime.value.text.split(':')[0]) > picked.hour) {
          _showAlertDialog('Анхаар', 'Эхлэх цаг, Дуусах цагаас бага байх ёстой.');
        } else if (int.parse(txtStartTime.value.text.split(':')[0]) == picked.hour) {
          if (int.parse(txtStartTime.value.text.split(':')[1]) == picked.minute) {
            _showAlertDialog('Анхаар', 'Эхлэх цаг, Дуусах цагаас бага байх ёстой.');
          }
        } else {
          setState(() {
            timeVar.text = (picked.hour < 10
                ? ('0' + picked.hour.toString())
                : picked.hour.toString()) +
                ':' +
                (picked.minute < 10
                    ? ('0' + picked.minute.toString())
                    : picked.minute.toString());
          });
        }
      }
    }
  }

  Future<void> _showAlertDialog(String title, String body) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(body)
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Хаах'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    Future.delayed(Duration.zero, () async {
      signedUID = ModalRoute.of(context).settings.arguments as String;

      ProgressDialog pr = ProgressDialog(context,
          type: ProgressDialogType.Normal,
          isDismissible: false,
          showLogs: false);
      pr.style(
          message: 'Уншиж байна...',
          borderRadius: 10.0,
          backgroundColor: Colors.white,
          progressWidget: CircularProgressIndicator(),
          elevation: 10.0,
          insetAnimCurve: Curves.easeInOut,
          progress: 0.0,
          maxProgress: 100.0,
          progressTextStyle: TextStyle(
              color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
          messageTextStyle: TextStyle(
              color: Colors.black,
              fontSize: 19.0,
              fontWeight: FontWeight.w600));
      pr.show();

      dynamic data = await getDivUserUIDs(signedUID);
      print(data);
      List<DropdownMenuItem<String>> loadedNames = List();
      for (dynamic i in data) {
        listOfUIDs.add(UserWithUID.fromJson(i));
        loadedNames.add(
          new DropdownMenuItem(
            value: listOfUIDs[listOfUIDs.length - 1].firstName,
            child: Text(listOfUIDs[listOfUIDs.length - 1].firstName),
          )
        );
      }
      if (loadedNames.length > 0) {
        setState(() {
          chosenUID = listOfUIDs[0].docID;
          dropdownValue = loadedNames[0].value;
          listDrpItem = loadedNames;
          pr.hide();
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
        appBar: AppBar(
          title: Text('Ирц бүртгэх'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16),
          child: Form(
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  child: DropdownButton(
                    value: dropdownValue,
                    icon: Icon(Icons.arrow_downward),
                    iconSize: 24,
                    elevation: 16,
                    style: TextStyle(color: Color(0xFF0D42CC)),
                    underline: Container(
                      height: 2,
                      color: Color(0xFF0D42CC),
                    ),
                    onChanged: (String newValue) {
                      int indexOfUID = listOfUIDs.indexWhere((element) => element.firstName == newValue);
                      setState(() {
                        dropdownValue = newValue;
                        chosenUID = listOfUIDs[indexOfUID].docID;
                      });
                    },
                    items: listDrpItem,
                  ),
                ),
                SizedBox(height: size.height * 0.02),
                TextFormField(
                  decoration:
                      InputDecoration(hintText: 'Огноо', labelText: 'Огноо'),
                  onTap: () => selectDate(context, txtDateTime),
                  controller: txtDateTime,
                  readOnly: true,
                ),
                SizedBox(height: size.height * 0.02),
                TextFormField(
                  decoration: InputDecoration(
                      hintText: 'Эхлэх цаг', labelText: 'Эхлэх цаг'),
                  onTap: () => selectTime(context, txtStartTime, 'start'),
                  controller: txtStartTime,
                  readOnly: true,
                ),
                SizedBox(height: size.height * 0.02),
                TextFormField(
                  decoration: InputDecoration(
                      hintText: 'Дуусах цаг', labelText: 'Дуусах цаг'),
                  onTap: () => selectTime(context, txtEndTime, 'end'),
                  controller: txtEndTime,
                  readOnly: true,
                ),
                SizedBox(height: size.height * 0.03),
                RoundedButton(
                  text: "Хадгалах",
                  press: () async {
                    saveAttendance(chosenUID);
                  },
                ),
              ],
            ),
          ),
        ));
  }
}
