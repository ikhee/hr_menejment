class UserWithUID {
  String docID;
  String firstName;
  String lastName;

  UserWithUID({this.docID, this.firstName, this.lastName});

  UserWithUID.fromJson(Map<String, dynamic> json)
    : docID = json['docID'],
      firstName = json['firstName'],
      lastName = json['lastName'];
}