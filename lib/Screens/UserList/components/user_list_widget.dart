import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;

import 'package:hr_menegment/Screens/UserList/components/detailUser.dart';
import 'package:hr_menegment/Screens/detailUser/detail_user_screen.dart';
import 'package:hr_menegment/components/progress_dialog.dart';

class UserListWidget extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<UserListWidget> {
  TextEditingController editingController = TextEditingController();

  final duplicateItems = List<String>.generate(100, (i) => "Item $i");
  List<String> items = List<String>();
  List<dynamic> users;
  String uid;

  List<DetailUser> detailUsers = List();

  getDivUserList(uid) async {
    final http.Response response = await http.post(
        'https://us-central1-hr-management-4c06c.cloudfunctions.net/getDivUserList',
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'uid': uid,
        }));
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      print('statusCode: ${response.statusCode} \n body: ${response.body}');
    }
  }

  @override
  void initState() {
    // items.addAll(duplicateItems);
    Future.delayed(Duration.zero, () async {
      uid = ModalRoute.of(context).settings.arguments as String;
      ProgressDialog pr = ProgressDialog(context,
          type: ProgressDialogType.Normal,
          isDismissible: false,
          showLogs: false);

      pr.style(
          message: 'Уншиж байна...',
          borderRadius: 10.0,
          backgroundColor: Colors.white,
          progressWidget: CircularProgressIndicator(),
          elevation: 10.0,
          insetAnimCurve: Curves.easeInOut,
          progress: 0.0,
          maxProgress: 100.0,
          progressTextStyle: TextStyle(
              color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
          messageTextStyle: TextStyle(
              color: Colors.black,
              fontSize: 19.0,
              fontWeight: FontWeight.w600));

      pr.show();
      dynamic data = await getDivUserList(uid);
      print(data);
      List<String> userNames = List<String>();
      users = data;
      for (dynamic i in data) {
        detailUsers.add(DetailUser.fromJson(i));
        if (detailUsers[detailUsers.length - 1].positionName == null) {
          detailUsers[detailUsers.length - 1].positionName = 'Хөгжүүлэгч';
        }
        userNames.add(i['lastName'] + ' ' + i['firstName']);
      }

      setState(() {
        items.addAll(userNames);
        pr.hide();
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('User_List')),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 8, 20, 8),
            child: TextField(
              onChanged: (value) {},
              controller: editingController,
              decoration: InputDecoration(
                labelText: "Хайх",
                hintText: "Хайх",
                prefixIcon: Icon(Icons.search),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(25)),
                ),
              ),
            ),
          ),
          Expanded(
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: items.length,
                itemBuilder: (content, index) {
                  return Card(
                    child: ListTile(
                      leading: CircleAvatar(
                        backgroundImage:
                            AssetImage('assets/images/profile.png'),
                      ),
                      title: Text(
                        '${items[index]}',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w500),
                      ),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                DetailUserScreen(user: detailUsers[index]),
                          ),
                        );
                      },
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30))),
                    color: Color.fromARGB(255, 13, 66, 204),
                    margin: EdgeInsets.fromLTRB(20, 5, 20, 5),
                  );
                }),
          )
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('Home')),
          BottomNavigationBarItem(
              icon: Icon(Icons.exit_to_app), title: Text('Гарах'))
        ],
      ),
    );
  }
}
