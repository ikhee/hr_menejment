import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class DetailUser{
  String empCode;
  String personCode;
  String firstName;
  String firstName2;
  String lastName;
  String lastName2;
  String personName;
  String personName2;
  String statusCode;
  String statusName;
  String statusName2;
  int workYear;
  int workYearInt;
  int insuranceType;
  String insuranceTypeName;
  String insuranceTypeName2;
  int locCode;
  int occCode;
  String occName;
  String occName2;
  int socInsYear;
  DateTime enteredDate;
  DateTime retiredDate;
  DateTime sackedDate;
  int createdBy;
  DateTime createdDatetime;
  int modifiedBy;
  DateTime modifiedDatetime;
  String companyCode;
  DateTime returnDate;
  int empTypeId;
  String empTypeName;
  String empTypeName2;
  String positionCode;
  String positionName;
  String positionName2;
  int rankId;
  String rankName;
  String rankName2;
  String chartCode;
  String chartName;
  String chartName2;
  String registerCode;
  String workEmail;
  String workPhone;
  String workIntPhone;
  DateTime firstDate;
  int workYearSector;
  int maternityYear;
  int intTermabsYear;
  int lastChartYear;
  int lastPositionYear;
  String locName;
  String locName2;
  DateTime mainEmpDate;
  int sexCode;
  String sexName;
  String sexName2;
  DateTime startDate;
  int regionCode;
  String regionName;
  String regionName2;
  String createdUserName;
  String modifiedUserName;
  String adlogOnPc;
  String phone;
  String permission;
  DetailUser(this.firstName, this.positionName);

  DetailUser.fromJson(Map<String, dynamic> json)
      : empCode = json['empCode'],
        personCode = json['personCode'],
        firstName = json['firstName'],
        firstName2 = json['firstName2'],
        lastName = json['lastName'],
        lastName2 = json['lastName2'],
        personName = json['personName'],
        personName2 = json['personName2'],
        statusCode = json['statusCode'],
        statusName = json['statusName'],
        statusName2 = json['statusName2'],
        workYear = json['workYear'],
        workYearInt = json['workYearInt'],
        insuranceType = json['insuranceType'],
        insuranceTypeName = json['insuranceTypeName'],
        insuranceTypeName2 = json['insuranceTypeName2'],
        locCode = json['locCode'],
        occCode = json['occCode'],
        occName = json['occName'],
        occName2 = json['occName2'],
        socInsYear = json['socInsYear'],
        enteredDate = json['enteredDate'],
        retiredDate = json['retiredDate'],
        sackedDate = json['sackedDate'],
        createdBy = json['createdBy'],
        createdDatetime = json['createdDatetime'],
        modifiedBy = json['modifiedBy'],
        modifiedDatetime = json['modifiedDatetime'],
        companyCode = json['companyCode'],
        returnDate = json['returnDate'],
        empTypeId = json['empTypeId'],
        empTypeName = json['empTypeName'],
        empTypeName2 = json['empTypeName2'],
        positionCode = json['positionCode'],
        positionName = json['positionName'],
        positionName2 = json['positionName2'],
        rankId = json['rankId'],
        rankName = json['rankName'],
        rankName2 = json['rankName2'],
        chartCode = json['chartCode'],
        chartName = json['chartName'],
        chartName2 = json['chartName2'],
        registerCode = json['registerCode'],
        workEmail = json['workEmail'],
        workPhone = json['workPhone'],
        workIntPhone = json['workIntPhone'],
        firstDate = json['firstDate'],
        workYearSector = json['workYearSector'],
        maternityYear = json['maternityYear'],
        intTermabsYear = json['intTermabsYear'],
        lastChartYear = json['lastChartYear'],
        lastPositionYear = json['lastPositionYear'],
        locName = json['locName'],
        locName2 = json['locName2'],
        mainEmpDate = json['mainEmpDate'],
        sexCode = json['sexCode'],
        sexName = json['sexName'],
        sexName2 = json['sexName2'],
        startDate = json['startDate'],
        regionCode = json['regionCode'],
        regionName = json['regionName'],
        regionName2 = json['regionName2'],
        createdUserName = json['createdUserName'],
        modifiedUserName = json['modifiedUserName'],
        adlogOnPc = json['adlogOnPc'],
        phone = json['phone'],
        permission = json['permission'];
}
