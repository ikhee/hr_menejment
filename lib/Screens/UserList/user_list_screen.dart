import 'package:flutter/material.dart';

import 'components/user_list_widget.dart';

class UserListScreen extends StatelessWidget {
  static String routeName = "/userList";

  @override
  Widget build(BuildContext context) {
    return UserListWidget();
  }
}
