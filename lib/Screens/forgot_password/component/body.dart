import 'package:flutter/material.dart';
import 'package:hr_menegment/components/column_surfix_icon.dart';
import 'package:hr_menegment/components/form_error.dart';
import 'package:hr_menegment/components/rounded_button.dart';

import '../../../constants.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
      width: double.infinity,
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: size.width * 0.08),
          child: Column(
            children: [
              SizedBox(height: size.height * 0.05),
              Text(
                "Нууц үг мартсан",
                style: TextStyle(
                  fontSize: 28,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                "Бүртгэлтэй мэйл хаягаа оруулна уу. \nТаны мэйл хаяг руу баталгаажуулах линк илгээх болно.",
                textAlign: TextAlign.center,
              ),
              ForgotPassForm(),
            ],
          ),
        ),
      ),
    );
  }
}

class ForgotPassForm extends StatefulWidget {
  @override
  _ForgotPassFormState createState() => _ForgotPassFormState();
}

class _ForgotPassFormState extends State<ForgotPassForm> {
  final _formKey = GlobalKey<FormState>();
  List<String> errors = [];
  FocusNode myFocusNode = new FocusNode();
  String email;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Column(
          children: [
            SizedBox(height: size.height * 0.15),
            TextFormField(
              focusNode: myFocusNode,
              keyboardType: TextInputType.emailAddress,
              onSaved: (newValue) => email = newValue,
              onChanged: (value) {
                if (value.isNotEmpty && errors.contains(kMailNullError)) {
                  setState(() {
                    errors.remove(kMailNullError);
                  });
                  return "";
                } else if (emailValidatorRegExp.hasMatch(value) &&
                    errors.contains(kInValidMailError)) {
                  setState(() {
                    errors.remove(kInValidMailError);
                  });
                }
              },
              validator: (value) {
                if (value.isEmpty && !errors.contains(kMailNullError)) {
                  setState(() {
                    errors.add(kMailNullError);
                  });
                  return "";
                } else if (!emailValidatorRegExp.hasMatch(value) &&
                    !errors.contains(kInValidMailError)) {
                  setState(() {
                    errors.add(kInValidMailError);
                  });
                }
                return null;
              },
              decoration: InputDecoration(
                labelText: "Мэйл хаяг",
                hintText: "Мэйл хаягаа оруулна уу.",
                // floatingLabelBehavior: FloatingLabelBehavior.always,
                suffixIcon: CustomSurfFixIcon(
                  svgIcon: "assets/icons/mail.svg",
                ),
              ),
            ),
            SizedBox(height: size.height * 0.15),
            FormError(errors: errors),
            RoundedButton(
              text: "Үргэлжлүүлэх",
              press: () {
                if (_formKey.currentState.validate()) {}
              },
            ),
          ],
        ),
      ),
    );
  }
}
