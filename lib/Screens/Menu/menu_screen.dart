import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:hr_menegment/Screens/Menu/components/menu.dart';

import 'package:hr_menegment/Screens/Menu/components/menu_item.dart';
import 'package:hr_menegment/Screens/Welcome/welcome_screen.dart';
import 'package:hr_menegment/components/progress_dialog.dart';
import 'components/dummy_data.dart';

class MenuScreen extends StatefulWidget {
  static String routeName = "/menu";

  @override
  _MenuScreenState createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {
  MenuScreenArgs args;

  List<MenuItem> menuItems = List<MenuItem>();


  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      args = ModalRoute.of(context).settings.arguments;
      setState(() {
        menuItems = args.listOfMenuItem;
        print(args.userCredential);
        print(args.listOfMenuItem);
      });
    });
    super.initState();
  }

  barItemSelected(i) {
    if (i == 1) {
      FirebaseAuth.instance.signOut();
      Navigator.popAndPushNamed(context, WelcomeScreen.routeName);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Menu'),
      ),
      body: GridView(
        padding: const EdgeInsets.all(25),
        children: menuItems,
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 200,
          childAspectRatio: 3 / 2,
          crossAxisSpacing: 20,
          mainAxisSpacing: 20,
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Home')
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.exit_to_app),
              title: Text('Гарах')
          )
        ],
        onTap: barItemSelected,
      ),
    );
  }
}

class MenuScreenArgs {
  final UserCredential userCredential;
  final List<MenuItem> listOfMenuItem;

  MenuScreenArgs({this.userCredential, this.listOfMenuItem});
}
