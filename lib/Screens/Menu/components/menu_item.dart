import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hr_menegment/Screens/Attendance/RequestAttend/generalAttendtReq.dart';
import 'package:hr_menegment/Screens/Attendance/attendance_widget.dart';
import 'package:hr_menegment/Screens/AttendanceDetail/AttendanceDetail.dart';
import 'package:hr_menegment/Screens/UserList/user_list_screen.dart';

class MenuItem extends StatelessWidget {
  final String scrId;
  final String title;
  final Color color;
  final LinearGradient linearGradient;
  final String uid;

  MenuItem({this.scrId, this.title, this.color, this.linearGradient, this.uid});

  void selectMenu(ctx) {
    print(scrId);
    if (scrId == 'listScreen1') {
      Navigator.of(ctx).pushNamed(UserListScreen.routeName, arguments: uid);
    } else if (scrId == 'attList') {
      Navigator.pushNamed(ctx, AttendanceList.routeName, arguments: uid);
    } else if (scrId == 'addAttendance') {
      Navigator.pushNamed(ctx, AttendanceDetail.routeName, arguments: uid);
    } else if (scrId == 'requestAttend') {
      Navigator.pushNamed(ctx, AttendGeneralReq.routeName);
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(15),
      onTap: () => selectMenu(context),
      child: Container(
        padding: const EdgeInsets.all(15),
        child: Center(
          child: Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 20,
                fontFamily: 'RobotoCondensed',
                fontWeight: FontWeight.values[7],
                color: Colors.white),
          ),
        ),
        decoration: BoxDecoration(
          gradient: linearGradient,
          borderRadius: BorderRadius.circular(15),
        ),
      ),
    );
  }
}
