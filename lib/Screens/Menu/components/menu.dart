import 'package:flutter/material.dart';

class Menu {
  final String id;
  final String title;
  final Color color;
  final LinearGradient linearGradient;

  const Menu({@required this.id, @required this.title, this.color = Colors.orange, this.linearGradient});
}