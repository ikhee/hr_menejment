import 'package:flutter/material.dart';

import 'package:hr_menegment/Screens/Menu/components/menu.dart';

const DUMMY_CATEGORIES = const [
  LinearGradient(colors: [
    Color.fromARGB(255, 229, 93, 135),
    Color.fromARGB(255, 239, 142, 56)
  ]),
  LinearGradient(colors: [
    Color.fromARGB(255, 13, 66, 204),
    Color.fromARGB(255, 111, 38, 184)
  ]),
  LinearGradient(colors: [
    Color.fromARGB(255, 0, 205, 253),
    Color.fromARGB(255, 146, 251, 17)
  ]),
  LinearGradient(colors: [
    Color.fromARGB(255, 58, 9, 134),
    Color.fromARGB(255, 137, 37, 62)
  ]),
];
