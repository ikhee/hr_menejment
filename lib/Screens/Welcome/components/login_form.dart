import 'dart:convert';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:hr_menegment/Screens/SignUp/sign_up.dart';
import 'package:http/http.dart' as http;
import 'package:hr_menegment/Screens/Menu/components/dummy_data.dart';
import 'package:hr_menegment/Screens/Menu/components/menu_item.dart';
import 'package:hr_menegment/Screens/Menu/menu_screen.dart';
import 'package:hr_menegment/components/column_surfix_icon.dart';
import 'package:hr_menegment/components/form_error.dart';
import 'package:hr_menegment/components/rounded_button.dart';
import 'package:hr_menegment/components/progress_dialog.dart';
import 'package:hr_menegment/constants.dart';

class SignForm extends StatefulWidget {
  @override
  _SignFormState createState() => _SignFormState();
}

class _SignFormState extends State<SignForm> {
  final _formKey = GlobalKey<FormState>();
  final List<String> errors = [];
  FocusNode myFocusNode = new FocusNode();
  String userName, password;
  Future<FirebaseApp> a = Firebase.initializeApp();
  UserCredential userCredential;

  getMenus(uid) async {
    final http.Response response = await http.post(
        'https://us-central1-hr-management-4c06c.cloudfunctions.net/getMenus',
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
        'uid': uid,
        })
    );
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      print('statusCode: ${response.statusCode} \n body: ${response.body}');
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Form(
      key: _formKey,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 5),
        width: size.width * 0.8,
        child: Column(
            children: [
              buildUserNameForm(),
              SizedBox(height: size.height * 0.013),
              buildPasswordForm(),
              SizedBox(height: size.height * 0.04),
              FormError(errors: errors),
              RoundedButton(
                text: "НЭВТРЭХ",
                press: () {
                  if (_formKey.currentState.validate()) {
                    _formKey.currentState.save();
                    checkInputUser(context);
                  }
                },
              ),
              SizedBox(height: size.height * 0.02),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () => Navigator.pushNamed(context, ForgotPasswordScreen.routeName),
                    child: Text(
                      "- Нууц үгээ мартсан уу? -",
                      style: TextStyle(
                        fontSize: size.height*0.025,
                        color: kPrimaryColor,
                      ),
                    ),
                  ),
                ],
              ),
            ]
        ),
      ),
    );
  }

  TextFormField buildPasswordForm() {
    return TextFormField(
      focusNode: myFocusNode,
      onSaved: (newValue) => password = newValue,
      obscureText: true,
      onChanged: (value) {
        if(value.isNotEmpty && errors.contains(kPassNullError)){
          setState(() {
            errors.remove(kPassNullError);
          });
          return "";
        }
      },
      validator: (value) {
        if (errors.length == 0) {
          if (value.isEmpty && !errors.contains(kPassNullError)) {
            setState(() {
              errors.add(kPassNullError);
            });
            return "";
          }
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Нууц үг",
        hintText: kPassNullError,
        // floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurfFixIcon(svgIcon: "assets/icons/padlock.svg",),
      ),
    );
  }

  TextFormField buildUserNameForm() {
    return TextFormField(
      onSaved: (newValue) => userName = newValue,
      onChanged: (value) {
        if(value.isNotEmpty && errors.contains(kUserNullError)){
          setState(() {
            errors.remove(kUserNullError);
          });
          return "";
        }
      },
      validator: (value) {
        if(value.isEmpty && !errors.contains(kUserNullError)) {
          setState(() {
            errors.add(kUserNullError);
          });
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Нэвтрэх нэр",
        hintText: "Нэвтрэх нэр оруулна уу.",
        // floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurfFixIcon(svgIcon: "assets/icons/user-profile.svg",),
      ),
    );
  }

  checkInputUser(BuildContext context) async{
    final http.Response response = await http.post(
        'https://us-central1-hr-management-4c06c.cloudfunctions.net/helloWorld'
    );
    // ProgressDialog pr = ProgressDialog(context);
    // pr = ProgressDialog(
    //   context,
    //   type: ProgressDialogType.Normal,
    //   isDismissible: true,
    //   /// your body here
    //   customBody: LinearProgressIndicator(
    //     valueColor: AlwaysStoppedAnimation<Color>(Colors.blueAccent),
    //     backgroundColor: Colors.white,
    //   ),
    // );
    ProgressDialog pr =  ProgressDialog(context,type: ProgressDialogType.Normal, isDismissible: false, showLogs: false);

    pr.style(
        message: 'Уншиж байна...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );
    await pr.show();
    // FirebaseFirestore.instance.collection('tbl_users').doc('iEsQpUvkkbPTUyeN4WLq').snapshots().listen((event) {
    //   print(event.data());
    // });

    try {
      userCredential = await FirebaseAuth.instance.signInWithEmailAndPassword(email: userName, password: password);

      if (errors.contains(notValidateUser)) {
        setState(() {
          errors.remove(notValidateUser);
        });
      }
      List<MenuItem> m = List<MenuItem>();

      dynamic data = await getMenus(userCredential.user.uid);
      print(data);
      int i = 0;
      for (dynamic s in (data['menu_items']) as List<dynamic>) {
        m.add(MenuItem(
          scrId: s['scr_id'].toString(),
          title: s['title'].toString(),
          linearGradient: DUMMY_CATEGORIES[i % 4],
          uid: userCredential.user.uid,
        ));
        i++;
      }
      await pr.hide();
      Navigator.popAndPushNamed(context, MenuScreen.routeName, arguments: MenuScreenArgs(userCredential: userCredential, listOfMenuItem: m));

      // FirebaseFirestore.instance.collection('clc_user').doc(userCredential.user.uid).get().then((value) {
      //   FirebaseFirestore.instance.collection('clc_menu').doc(value.data()['permission']).get().then((value) async {
      //     int i = 0;
      //     for (dynamic s in (value.data()['menu_items'] as List<dynamic>)) {
      //       m.add(MenuItem(
      //         scrId: s['scr_id'].toString(),
      //         title: s['title'].toString(),
      //         linearGradient: DUMMY_CATEGORIES[i % 4],
      //         uid: userCredential.user.uid,
      //       ));
      //       i++;
      //     }
      //     await pr.hide();
      //     Navigator.popAndPushNamed(context, MenuScreen.routeName, arguments: MenuScreenArgs(userCredential: userCredential, listOfMenuItem: m));
      //   });
      // });

    } on FirebaseAuthException catch (e) {
      await pr.hide();
      if (e.code == 'user-not-found') {
        print('No user found for that email.');
      } else if (e.code == 'wrong-password') {
        print('Wrong password provided for that user.');
      }
      if (errors.length == 0){
        if (!errors.contains(notValidateUser)) {
          setState(() {
            errors.add(notValidateUser);
          });
        }
      }
    }
  }
}