import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hr_menegment/constants.dart';

import 'background.dart';
import 'login_form.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                appName,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(height: size.height * 0.05),
              Image.asset("assets/icons/company_logo.png",
                height: size.height * 0.2,
              ),
              SizedBox(height: size.height * 0.05),
              SignForm(),
            ],
          ),
        ),
      )
    );
  }
}
