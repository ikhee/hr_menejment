import 'constants.dart';
import 'package:flutter/material.dart';


ThemeData theme() {
  return ThemeData(
      primaryColor: kPrimaryColor,
      scaffoldBackgroundColor: Colors.white,
      appBarTheme: appBarTheme(),
      inputDecorationTheme: inputDecorationTheme(),
      fontFamily: 'RaleWay',
      textTheme: ThemeData.light().textTheme.copyWith(
          title: TextStyle(
              fontSize: 20,
              fontFamily: 'RobotoCondensed',
              fontWeight: FontWeight.bold)
      )
  );
}

AppBarTheme appBarTheme() {
  return AppBarTheme(
    color: Colors.transparent,
    elevation: 0,
    brightness: Brightness.light,
    iconTheme: IconThemeData(color: Colors.blueAccent),
    textTheme: TextTheme(
      headline6: TextStyle(
        color: kPrimaryColor,
        fontSize: 18,
      ),
    ),
  );
}

InputDecorationTheme inputDecorationTheme() {
  OutlineInputBorder outlineInputBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(30),
      borderSide: BorderSide(color: kPrimaryColor),
      gapPadding: 6);
  return InputDecorationTheme(
    // floatingLabelBehavior: FloatingLabelBehavior.always,
    hintStyle: TextStyle(fontSize: 15),
    contentPadding: EdgeInsets.symmetric(
      horizontal: 35,
    ),
    alignLabelWithHint: true,
    enabledBorder: outlineInputBorder,
    focusedBorder: outlineInputBorder,
    border: outlineInputBorder,
  );
}

TextTheme textTheme() {
  return TextTheme(
    bodyText1: TextStyle(
      color: kPrimaryColor,
    ),
  );
}