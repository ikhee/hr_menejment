import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class FormError extends StatelessWidget {
  const FormError({
    Key key,
    @required this.errors,
  }) : super(key: key);
  final List<String> errors;
  static Size size;

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Column(
        children: List.generate(
          errors.length, (index) => formErrorText(error: errors[index]),
        )
    );
  }
  Row formErrorText({String error}) {
    return Row(children: [
      SvgPicture.asset(
        "assets/icons/close.svg",
        width: size.width * 0.03,
        height: size.width * 0.03,
      ),
      SizedBox(width: size.width * 0.05),
      Text(error),
    ]);
  }
}