import 'package:flutter/material.dart';

import '../constants.dart';
class RoundedButton extends StatelessWidget {
  final String text;
  final Function press;
  final Color color, textColor;

  const RoundedButton({
    Key key,
    this.text,
    this.press,
    this.color = kPrimaryColor,
    this.textColor = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
        margin: EdgeInsets.symmetric(vertical: 5),
        width: size.width * 0.8,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(29),
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(80)
            ),
            padding: EdgeInsets.all(0),
            onPressed: press,
            // color: color,
            child: Ink(
              decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
                  kPrimaryColor, kPrimaryColor2
                ],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight
                ),
                  borderRadius: BorderRadius.circular(30.0),
              ),
              child: Container(
                constraints: BoxConstraints(maxWidth: 350.0, minHeight: 50.0),
                alignment: Alignment.center,
                child: Text(
                  text,
                  style: TextStyle(color: textColor),
                ),
              ),
            )
          ),
        )
    );
  }
}