class DropdownData {
  final String id;
  final String name;

  DropdownData(this.id, this.name);
}