class DateObject {
  int day, year, month, minute, hour;
  final DateTime dateTime;

  DateObject(this.dateTime){
    day = dateTime.day;
    year = dateTime.year;
    month = dateTime.month;
    hour =dateTime.hour;
    minute = dateTime.minute;
  }

  String getDateString() {
    return year.toString() + "/" + month.toString() + "/" + day.toString();
  }

  String getDatePickerString() {
    return year.toString() + "-" + month.toString() + "-" + day.toString();
  }

  String getTimeMinuteString() {
    return hour.toString() + ":" + minute.toString();
  }
}