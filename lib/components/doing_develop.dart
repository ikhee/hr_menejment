import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hr_menegment/constants.dart';

class DoingDevelop extends StatelessWidget {
  const DoingDevelop({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
        padding: new EdgeInsets.only(top: size.height*0.25, left: 16, right: 16),
        child: Card(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ListTile(
                leading: Icon(
                  IconData(0xe700, fontFamily: 'MaterialIcons'),
                  color: Colors.red,
                ),
                title: Text("Уучлаарай. Хөгжүүлэлт хийгдэж байна.",
                    style: new TextStyle(
                        fontStyle: FontStyle.italic,
                        fontWeight: FontWeight.w600,
                        fontSize: 20,
                        fontFamily: 'RobotoMono')),
              ),
              SizedBox(height: 16,),
              Text(
                "Хөгжүүлэлтийн багийхантай"
                    " холбоо барих:"
              ),
              SizedBox(height: 8,),
              Text(
                  "99111111",
                style: TextStyle(
                  color: kPrimaryColor,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 10,),
              Text(""),
            ],
          ),
        ));
  }
}
