import 'package:flutter_svg/svg.dart';
import 'package:flutter/material.dart';

class CustomSurfFixIcon extends StatelessWidget {
  const CustomSurfFixIcon({
    Key key, this.svgIcon,
  }) : super(key: key);

  final String svgIcon;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 20, 20, 20),
      child: SvgPicture.asset(
        svgIcon,
        height: 20,
      ),
    );
  }
}